module.exports = function (grunt) {
    "use strict";

    var pathConfig = {
        productionPath: "build/prod/",
        developPath: "src/libs/",
        bowerComponentsPath: "bower_components/"
    };

    // Задачи
    grunt.initConfig({


        // копируем библиотеки
        copy: {
            fonts: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'bootstrap/fonts/',
                        src: ['*'],
                        dest: pathConfig.productionPath + "fonts/"
                    },

                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'bootstrap/fonts/',
                        src: ['*'],
                        dest: pathConfig.developPath + "fonts/"
                    }
                ]
            },
            bower_components: {
                files: [
                    // Production version
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular/',
                        src: ['angular.min.js', 'angular.min.js.map'],
                        dest: pathConfig.productionPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-resource/',
                        src: ['angular-resource.min.js', 'angular-resource.min.js.map'],
                        dest: pathConfig.productionPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-ui-router/release/',
                        src: ['*.min.js'],
                        dest: pathConfig.productionPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-ui-mask/dist/',
                        src: ['*.min.js'],
                        dest: pathConfig.productionPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-bootstrap/',
                        src: ['*.min.js'],
                        dest: pathConfig.productionPath + "bootstrap/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: '',
                        src: ['index.html'],
                        dest: pathConfig.productionPath
                    },
                    {
                        expand: true,
                        src: "src/app/**/*.html",
                        dest: pathConfig.productionPath
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'jquery/dist/',
                        src: ['*.min.*'],
                        dest: pathConfig.productionPath + "jquery/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'bootstrap/dist/',
                        src: ['*/*.min.*'],
                        dest: pathConfig.productionPath + "bootstrap/"
                    },

                    //Development version
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular/',
                        src: ['angular.js'],
                        dest: pathConfig.developPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-resource/',
                        src: ['angular-resource.js'],
                        dest: pathConfig.developPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-ui-router/release/',
                        src: ['*.js', '!*.min.j*s'],
                        dest: pathConfig.developPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-ui-mask/dist/',
                        src: ['*.js', '!*.min.j*s'],
                        dest: pathConfig.developPath + "angular/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'angular-bootstrap/',
                        src: ['*.js', '!*.min.j*s', '*.css'],
                        dest: pathConfig.developPath + "bootstrap/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'jquery/dist/',
                        src: ['*.js', '!*.min.*'],
                        dest: pathConfig.developPath + "jquery/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: pathConfig.bowerComponentsPath + 'bootstrap/dist/',
                        src: ['*/*.js', '*/*.css', '*/*.map', '!*/*.min.*', '!*/npm.js'],
                        dest: pathConfig.developPath + "bootstrap/"
                    }
                ]
            }
        },


        // Склеиваем
        concat: {
            options: {
                separator: ';'
            },
            libs: {
                src: pathConfig.developPath + '**/*.js',
                dest: 'build/temp/libs.js'
            },
            main: {
                src: [
                    'src/app/**/*.js', '!src/app/server.js'  /// Все JS-файлы в папке кроме сервера
                ],
                dest: 'build/temp/scripts.js'
            }
        },

        // Сжимаем наши скрипты
        uglify: {
            libs: {
                files: {
                    // Результат задачи concat
                    'build/prod/libs.min.js': '<%= concat.libs.dest %>'
                }
            },
            main: {
                files: {
                    // Результат задачи concat
                    'build/prod/scripts.min.js': '<%= concat.main.dest %>'
                }
            }
        },


        // собираем css из less файлов
        less: {
            build: {
                src: 'src/css/*.less',
                dest: 'build/temp/css/style.css'
            }

        },

        // минимизируем css
        cssmin: {
            options: {
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'build/temp/css',
                    src: ['style.css', '!*.min.css'],
                    dest: pathConfig.productionPath + 'css',
                    ext: '.min.css'
                }]
            },
            uiBootstap: {
                files: [{
                    expand: true,
                    cwd: 'src/libs/bootstrap/',
                    src: ['ui-bootstrap-csp*'],
                    dest: pathConfig.productionPath + 'bootstrap/',
                    ext: '.min.css'
                }]
            }
        },


        /*jshint: {
         files: ['TestFactory.js', 'src/!**!/!*.js', 'test/!**!/!*.js'],
         options: {
         globals: {
         jQuery: true
         }
         }
         },*/
        watch: {
            concat: {
                files: '<%= concat.main.src %>',
                tasks: ['concat']
            }

        },

        useminPrepare: {
            html: 'index.html',
            options: {
                dest: 'build/prod'
            }
        },

        usemin: {html: ['<%= useminPrepare.options.dest %>/index.html']}


    });

    // Загрузка плагинов, установленных с помощью npm install
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Задача по умолчанию
    grunt.registerTask('default', ['less', 'cssmin', 'copy', 'concat', 'uglify', 'watch']);
    grunt.registerTask('prod', ['less', 'cssmin', 'copy', 'concat', 'uglify', 'usemin']);

    //initConfiggrunt.initConfig({
    //    jshint: {
    //        files: ['TestFactory.js', 'src/**/*.js', 'test/**/*.js'],
    //        options: {
    //            globals: {
    //                jQuery: true
    //            }
    //        }
    //    },
    //    watch: {
    //        files: ['<%= jshint.files %>'],
    //        tasks: ['jshint']
    //    }
    //});
    //
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //
    //grunt.registerTask('default', ['jshint']);module

};