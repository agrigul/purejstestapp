/**
 * Created by agriguletskiy on 17.07.2015.
 */
"use strict";


var express = require('express');
var app = express();

var allowCrossDomain = function (req, res, next) {

    console.log("called url: %s", req.url);

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
        res.send(200);
    }
    else {
        next();
    }
};


app.use('/', allowCrossDomain);
app.get('/api/products', function (req, res) {
    console.log("1");
    var vm = {};

    vm.products = [
        {
            "productId": 1,
            "productName": "Leaf rake",
            "productCode": "DN-00011",
            "releaseDate": "March 19, 2009",
            "description": "Leafe rake with 48....",
            "price": 19.95
        },
        {
            "productId": 2,
            "productName": "Garden Cart",
            "productCode": "DN-00023",
            "releaseDate": "March 19, 2009",
            "description": "Garden Cart.",
            "price": 32.99
        },
        {
            "productId": 3,
            "productName": "Hammer",
            "productCode": "DN-00048",
            "releaseDate": "March 19, 2009",
            "description": "Hammer rake with 48....",
            "price": 21.95
        },
        {
            "productId": 4,
            "productName": "Something else",
            "productCode": "DN-00014",
            "releaseDate": "March 19, 2009",
            "description": "Something else rake with 48....",
            "price": 41.95
        }


    ];
    if(req.param("search") || req.param("id")){
        res.json({
            "productId": 4,
            "productName": "Something else",
            "productCode": "DN-00014",
            "releaseDate": "March 19, 2009",
            "description": "Something else rake with 48....",
            "price": 41.95
        });
    }


    res.json(vm.products);
});


app.get('/api/products/:search', function (req, res) {
    console.log("2");
    var result = [];
    if (req.params.search === "some") {
        result = [{
            "productId": 4,
            "productName": "Something else",
            "productCode": "DN-00014",
            "releaseDate": "March 19, 2009",
            "description": "Something else rake with 48....",
            "price": 41.95
        }];
    }

    res.json(result);

});

app.get('/api/customers/:id', function (req, res) {

});


app.listen(8080, function () {
    console.log("//-------Node.js server running on port: 8080");
})
