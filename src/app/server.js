/**
 * Created by agriguletskiy on 17.07.2015.
 */
"use strict";


var express = require('express');
var app = express();

var allowCrossDomain = function (req, res, next) {

    console.log("called url: %s", req.url);

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
        res.send(200);
    }
    else {
        next();
    }
};


var products = [];


app.use('/', allowCrossDomain);
app.get('/api/products', function (req, res) {
    console.log("1");
    var vm = {};

    vm.products = products;
    if (req.param("search") || req.param("productId")) {
        if (req.param("productId")) {
            var id = req.param("productId") - 1;
            res.json(vm.products[id]);
        }
        if (req.param("search")) {
            res.json(vm.products.slice(0, 3));
        }


    } else {
        res.json(vm.products);
    }

});
app.post('/api/products', function (req, res) {
    console.log("2")
    res.send("ok");
});


app.get('/api/products/:productId', function (req, res) {
    console.log("3");
    var result = [];
    if (req.params.productId) {
        result = products[req.params.productId - 1];
    }

    res.json(result);

});


app.get('/api/products/:search', function (req, res) {
    console.log("4");
    console.log(req.params.search);
    var result = [];
    if (req.params.search === "some") {
        result[0] = products[0];
        result[1] = products[1];
        result[2] = products[2];
    }

    res.json(result);

});

app.post('/api/account/register', function (req, res) {

    res.send("OK");
})

app.listen(8080, function () {
    console.log("//-------Node.js server running on port: 8080");
})


products = [
    {
        "productId": 1,
        "productName": "Leaf rake",
        "productCode": "DN-00011",
        "releaseDate": "March 19, 2009",
        "description": "Leafe rake with 48....description",
        "price": 19.95,
        "cost": 17.20,
        "tags": ['leaf']
    },
    {
        "productId": 2,
        "productName": "Garden Cart",
        "productCode": "DN-00023",
        "releaseDate": "March 19, 2009",
        "description": "Garden Cart description....description",
        "price": 32.99,
        "cost": 25.20,
        "tags": ['garden']
    },
    {
        "productId": 3,
        "productName": "Hammer",
        "productCode": "DN-00048",
        "releaseDate": "March 19, 2009",
        "description": "Hammer rake with 48....description",
        "price": 21.95,
        "cost": 15.00,
        "tags": ['hammer']
    },
    {
        "productId": 4,
        "productName": "Something else",
        "productCode": "DN-00014",
        "releaseDate": "March 19, 2009",
        "description": "Something else rake with 48....description",
        "price": 41.95,
        "cost": 40.00,
        "tags": ['something']
    }


]
