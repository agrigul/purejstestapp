(function () {
    "use strict";

    angular.module("common.services")
        .factory("userAccount", ["$resource", "appSettings", UserAccount]);

    function UserAccount($resource, appSettings) {


        function registraction() {
            return $resource(appSettings.serverPath + "/api/Account/Register", null, {
                'registerUser': {method: 'POST'}
            });
        }


        function login() {
            return $resource(appSettings.serverPath + "/Token", null, {
                'loginUser': {
                    method: 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    transformRequest: function (data) {
                        var str = [];
                        for (var d in data) {
                            str.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]))
                        }

                        return str.join("&");
                    }

                }

            });
        }

        return {
            registraction: registraction,
            login: login
        };

    }
})();
