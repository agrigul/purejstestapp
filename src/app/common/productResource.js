/**
 * Created by agriguletskiy on 17.07.2015.
 */

(function () {
    "use strict";

    angular
        .module("common.services")
        .factory("productResource", ["$resource", "appSettings", ProductResource]);

    function ProductResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/products/:productId",
            null,
            {'update': {method: 'PUT'}}
        );
    }


}());