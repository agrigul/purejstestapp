(function () {
    "use strict";

    angular.module("productManagement")
        .controller("mainController", ["userAccount", MainController]);

    function MainController(userAccount) {
        var vm = this;
        vm.userData = {
            userName: '',
            email: '',
            password: '',
            confirmPassword: ''

        }
        vm.isLoggedIn = false;

        vm.login = function () {
            if (vm.userData.email && vm.userData.password) {
                vm.isLoggedIn = true;
            } else {
                vm.message = "wrong name or password";
            }

            vm.userData.grant_type = "password";
            vm.userData.userName = vm.userData.email;

            userAccount.login().loginUser(vm.userData,
                function (data) {
                    vm.isLoggedIn = "";
                    vm.message = "";
                    vm.password = "";
                    vm.token = data.access_token;
                },
                function (response) {
                    vm.password = "";
                    vm.isLoggedIn = false;
                    vm.message = response.statusText + "\r\n";
                    if (response.data.exceptionMessage) {
                        vm.message += response.data.exceptionMessage;
                    }
                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            vm.message += response.data.modelState[key] + "\r\n";
                        }
                    }

                })
        };

        vm.registerUser = function () {

            vm.userData.confirmPassword = vm.userData.password;

            userAccount.registraction.registerUser(vm.userData,
                function () {
                    vm.confirmPassword = "";
                    vm.message = "...registraction successful";
                    vm.login();
                },
                function (response) {
                    vm.isLoggedIn = false;
                    vm.message = response.statusText + "\r\n";
                    if (response.data.exceptionMessage) {
                        vm.message += response.data.exceptionMessage;
                    }
                    if (response.data.modelState) {
                        for (var key in response.data.modelState) {
                            vm.message += response.data.modelState[key] + "\r\n";
                        }
                    }
                });

        };

    }
})();
