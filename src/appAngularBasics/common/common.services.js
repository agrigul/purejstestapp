/**
 * Created by agriguletskiy on 17.07.2015.
 */
(function(){
    "use strict"

    angular.module("common.services", ["ngResource"])// ngResource - reference to angular-rousrce.js for $resource
        .constant("appSettings", { serverPath: "http://localhost:8080"}); // serverPath - path to backend with Web Api

}());