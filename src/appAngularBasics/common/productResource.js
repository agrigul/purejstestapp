/**
 * Created by agriguletskiy on 17.07.2015.
 */

(function () {
    "use strict";


    function ProductResource($resource, appSettings) {
        return $resource(appSettings.serverPath + "/api/products/:search");
    }
    angular
        .module("common.services")
        .factory("productResource", ["$resource", "appSettings", ProductResource]);


}());