/**
 * Created by agriguletskiy on 16.07.2015.
 */
(function () {
    "use strict";

    function ProductListController(productResourceRepository) {
        var vm = this;
        //vm.searchCriteria = 'some';
        productResourceRepository.query({search: vm.searchCriteria}, function (data) {
            vm.products = data;
        });

        this.Alert = function () {
            alert("alert!");
        };
    }

    ProductListController.prototype.ProtoAlert = function () {
        alert("proto");
    }

    angular.module("productManagement")
        .controller("productListController", ["productResource", ProductListController]);


}());