/**
 * Created by agriguletskiy on 17.07.2015.
 */
(function () {
    "use strict";


    angular.module("productManagement")
        .controller("productEditController",
        ["product",
            "$state",
            "productService",
            productEditController]);

    function productEditController(product, $state, productService) {
        var vm = this;
        vm.product = product;
        vm.priceOption = "percent";

        if (!(vm.product && vm.product.productId)) {
            vm.title = "New product";
        } else {
            vm.title = "Edit:" + vm.product.productName;
        }


        vm.marginPercent = function () {
            return productService.calculateMarginPercent(vm.product.price, vm.product.cost);
        };

        vm.calculatePrice = function () {
            var price = 0;
            if (vm.priceOption === 'amount') {
                price = productService.calculatePriceFromMarkupAmount(vm.product.cost, vm.markupAmount);
            }

            if (vm.priceOption === 'percent') {
                price = productService.calculatePriceFromMarkupPercent(vm.product.cost, vm.markupPercent);
            }


            vm.product.price = price;
        };

        vm.open = function ($event) {
            console.log($event);
            $event.preventDefault();
            $event.stopPropagation();
            vm.opened = !vm.opened;
        };

        vm.submit = function (isValid) {
            if (isValid) {
                vm.product.$save(function (data) {
                    toastr.success("Save Successful");
                });
            } else {
                toastr.error("Please fill required fields");
            }
        };

        vm.cancel = function () {
            $state.go("productListState");
        };

        vm.addTags = function (tags) {
            if (tags) {
                var array = tags.split(',');
                vm.product.tags = vm.product.tags ? vm.product.tags.concat(array) : array;
                vm.newTags = "";
            } else {
                alert("please enter one or more tags");
            }

        };
        vm.removeTag = function (index) {
            vm.product.tags.splice(index, 1);
        };


    }


}());