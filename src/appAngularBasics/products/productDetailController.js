/**
 * Created by agriguletskiy on 20.07.2015.
 */
(function () {
    "use strict"

    function ProductDetailController(product, productService) {

        var vm = this;
        vm.product = product;

        vm.title = "Product Detail" + vm.product.productName;

        if (vm.product.tags) {
            vm.product.tagList = vm.product.tags.toString();
        }

        vm.marginPercent = productService.calculateMarginPercent(vm.product.price, vm.product.cost);


    }

    angular
        .module("productManagement")
        .controller("productDetailController", ["product", "productService", ProductDetailController]);


}());
