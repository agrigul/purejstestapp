/**
 * Created by agriguletskiy on 16.07.2015.
 */
(function () {
    "use strict";
    var appFilesPath = 'src/app/';

    var app = angular.module("productManagement",
        ["common.services",
            "ui.router",
            "ui.mask",
            "ui.bootstrap"]);

    // global exception hadler
    app.config(function ($provide) {
        $provide.decorator("$exceptionHandler",
            ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        exception.message = "Please contact admin" + exception.message;
                        $delegate(exception, cause); // logs exception
                        alert(exception.message);
                    }
                }])

    });


    app.config(["$stateProvider", "$urlRouterProvider",
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise("/");

                $stateProvider
                    .state("homeState", {
                        url: "/",
                        templateUrl: appFilesPath + "home/welcomeView.html"
                    })
                    .state("productListState", {
                        url: "/products",
                        templateUrl: appFilesPath + "products/productListView.html",
                        controller: "productListController as vm"
                    })
                    .state("productDetailState", {
                        url: "/products/:productId",
                        templateUrl: appFilesPath + "products/productDetailView.html",
                        controller: "productDetailController",
                        controllerAs: "vm",
                        resolve: {
                            productResource: "productResource",
                            product: ['productResource', '$stateParams', function (productResource, $stateParams) {
                                var id = $stateParams.productId;
                                return productResource.get({productId: id}).$promise;

                            }]
                        }
                    })
                    .state("productEditState", {
                        abstract: true,
                        url: "/products/edit/:productId",
                        templateUrl: appFilesPath + "products/productEditView.html",
                        controller: "productEditController",
                        controllerAs: "vm",
                        resolve: {
                            productResource: "productResource",
                            product: ['productResource', '$stateParams', function (productResource, $stateParams) {
                                var id = $stateParams.productId;
                                return productResource.get({productId: id}).$promise;

                            }]
                        }
                    })
                    .state("productEditState.info", {
                        url: "/info",
                        templateUrl: appFilesPath + "products/_productEditInfoView.html"
                    })
                    .state("productEditState.price", {
                        url: "/price",
                        templateUrl: appFilesPath + "products/_productEditPriceView.html"
                    })
                    .state("productEditState.tags", {
                        url: "/tags",
                        templateUrl: appFilesPath + "products/_productEditTagsView.html"
                    })
                ;
            }]
    );
}());